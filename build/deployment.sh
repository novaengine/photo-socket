# Authenticate with doctl
doctl auth init -t="$DOCTL_TOKEN"
doctl kubernetes cluster kubeconfig save $CLUSTER_ID


kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=${DOCKER_USER} --docker-password=${DOCKER_PASSWORD} --docker-email=valeness84@gmail.com

apk update && apk add gettext

# Do the deployment
cat build/deployment.yml | envsubst | kubectl apply -f -
cat build/service.yml | envsubst | kubectl apply -f -
kubectl rollout restart deployment photo-socket