FROM golang:1.15

WORKDIR /usr/src/myapp

COPY . .

RUN chmod +x build/entrypoint.sh
RUN go get

CMD ["./build/entrypoint.sh"]