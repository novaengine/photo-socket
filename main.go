package main

import (
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
	"os"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
)

type User struct {
	ID   string `json:"id"`
	conn *websocket.Conn
}

type Store struct {
	Users []*User
	sync.Mutex
}

type Message struct {
	DeliveryID string `json:"id"`
	Content    string `json:"content"`
}

var (
	gStore      *Store
	gPubSubConn *redis.PubSubConn
	gRedisConn  = func() (redis.Conn, error) {
		return redis.Dial("tcp", "ps-redis-service:6379")
	}
)

func init() {
	gStore = &Store{
		Users: make([]*User, 0, 1),
	}
}

func (s *Store) newUser(conn *websocket.Conn, id string) *User {
	u := &User{
		ID:   id,
		conn: conn,
	}

	if err := gPubSubConn.Subscribe(u.ID); err != nil {
		panic(err)
	}
	s.Lock()
	defer s.Unlock()

	s.Users = append(s.Users, u)
	return u
}

func deliverMessages() {
	for {
		switch v := gPubSubConn.Receive().(type) {
		case redis.Message:
			gStore.findAndDeliver(v.Channel, string(v.Data))

		case redis.Subscription:
			log.Printf("subscription message: %s: %s %d\n", v.Channel, v.Kind, v.Count)

		case error:
			log.Println("error pub/sub, delivery has stopped")
			return
		}
	}
}

func (s *Store) findAndDeliver(userID string, content string) {
	m := Message{
		Content: content,
	}
	for _, u := range s.Users {
		if u.ID == userID {
			if err := u.conn.WriteJSON(m); err != nil {
				log.Printf("error on message delivery e: %s\n", err)
			} else {
				log.Printf("user %s found, message sent\n", userID)
			}
			return
		}
	}
	log.Printf("user %s not found at our store\n", userID)
}

var serverAddress = ":8080"

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func wsHandler(w http.ResponseWriter, r *http.Request) {

	cookie, _ := r.Cookie("X-Authorization")

    apiUrl := os.Getenv("API_URL")

    log.Println(apiUrl)

	client := &http.Client{}
	// Attempt to Authorize
    req, err := http.NewRequest("GET", apiUrl + "/validate", nil)
	req.Header.Add("Authorization", "Bearer " + cookie.Value)
	resp, err := client.Do(req)

	if resp.StatusCode != http.StatusOK {
		return
	}

	nu := User{}
	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
			return
		}

		err = json.Unmarshal(bodyBytes, &nu)
		if err != nil {
			log.Fatal(err)
			return
		}
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("upgrader error %s\n" + err.Error())
		return
	}

	for _, u := range gStore.Users {
		if u.ID == nu.ID {

			u.conn = conn
		}
	}

	u := gStore.newUser(conn, nu.ID)
	log.Printf("user %s joined\n", nu.ID)

	m := Message{
		Content: "{\"user_id\" : \""+u.ID+"\"}",
	}

	u.conn.WriteJSON(m)

	for {
		var m Message
		if err := u.conn.ReadJSON(&m); err != nil {
			log.Printf("error on ws. message %s\n", err)
			break
		}
		if c, err := gRedisConn(); err != nil {
			log.Printf("error on redis conn. %s\n", err)
			break

		} else {
			log.Println(string(m.Content))
			c.Do("PUBLISH", m.DeliveryID, string(m.Content))
		}
	}
}

func loadEnv() {
    err := godotenv.Load()
    if err != nil {
        log.Fatal("Error loading .env file")
    }
}

func main() {

    loadEnv()

	gRedisConn, err := gRedisConn()
	if err != nil {
		panic(err)
	}
	defer gRedisConn.Close()
	gPubSubConn = &redis.PubSubConn{Conn: gRedisConn}
	defer gPubSubConn.Close()
	go deliverMessages()
	http.HandleFunc("/ws", wsHandler)
	log.Printf("server started at %s\n", serverAddress)

	log.Fatal(http.ListenAndServe(serverAddress, nil))
}