module gitlab.com/valeness/photo-socket

go 1.15

require (
	github.com/go-redis/redis/v8 v8.2.3 // indirect
	github.com/gomodule/redigo v1.8.2 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
)
